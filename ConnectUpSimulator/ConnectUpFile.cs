﻿namespace ConnectUpSimulator
{
    public class ConnectUpFile
    {   
        public int OperationId { get; set ; }
        public string Instrument { get; set; }
        public string Analyse { get; set; }
        public string Filename { get; set; }
        public string InstrumentSectionId { get; set; }
        public string DateTime { get; set; }
        public string IdFile { get; set; }
    }
}
