﻿using System.Configuration;
using System.Windows;
using System.Windows.Forms;

namespace ConnectUpSimulator
{
    /// <summary>
    /// Logique d'interaction pour option.xaml
    /// </summary>
    public partial class Option : Window
    {
        public Option()
        {
            InitializeComponent();
        }

        public void OpenMainWindow(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Path.Text = ConfigurationManager.AppSettings["Path"].ToString();
            IpGeneup.Text = ConfigurationManager.AppSettings["IpGeneup"].ToString();
            IpTempo.Text = ConfigurationManager.AppSettings["IpTempo"].ToString();
            IpVidas.Text = ConfigurationManager.AppSettings["IpVidas"].ToString();
        }

        public void Save(object sender, RoutedEventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            config.AppSettings.Settings["Path"].Value = Path.Text;
            config.AppSettings.Settings["IpGeneup"].Value = IpGeneup.Text;
            config.AppSettings.Settings["IpTempo"].Value = IpTempo.Text;
            config.AppSettings.Settings["IpVidas"].Value = IpVidas.Text;

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");

            string optionsSaved = "Saved succesfully !";
            string caption = "Success";
            MessageBoxButton button = MessageBoxButton.OK;
            MessageBoxImage icon = MessageBoxImage.Information;
            System.Windows.MessageBox.Show(optionsSaved, caption, button, icon);
        }

        public void GetFolderPath(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            folderDialog.ShowNewFolderButton = false;

            DialogResult result = folderDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            { 
                string sPath = folderDialog.SelectedPath;
                Path.Text = sPath;
            }
        }
    }
}
