﻿namespace ConnectUpSimulator
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Input;
    using System.Xml.Linq;
    using System.Xml.XPath;

    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BackgroundWorker worker = new BackgroundWorker();

        ObservableCollection<ConnectUpFile> connectUpfilesBw = new ObservableCollection<ConnectUpFile>();

        public ObservableCollection<ConnectUpFile> ConnectUpFiles { get; set; }
        public ObservableCollection<ConnectUpFile> ConnectUpFilesDilumat { get; set; }
        public ObservableCollection<ConnectUpFile> ConnectUpFilesGeneup { get; set; }
        public ObservableCollection<ConnectUpFile> ConnectUpFilesTempo { get; set; }
        public ObservableCollection<ConnectUpFile> ConnectUpFilesVidas { get; set; }

        public ConnectUpFile SelectedRow { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = this;
            this.ConnectUpFiles = new ObservableCollection<ConnectUpFile>();
            this.ConnectUpFilesDilumat = new ObservableCollection<ConnectUpFile>();
            this.ConnectUpFilesGeneup = new ObservableCollection<ConnectUpFile>();
            this.ConnectUpFilesTempo = new ObservableCollection<ConnectUpFile>();
            this.ConnectUpFilesVidas = new ObservableCollection<ConnectUpFile>();
        }

        private void Refresh(object sender, RoutedEventArgs e)
        {
            StartLoad();

            ConnectUpFiles.Clear();
            ConnectUpFilesDilumat.Clear();
            ConnectUpFilesGeneup.Clear();
            ConnectUpFilesTempo.Clear();
            ConnectUpFilesVidas.Clear();

            Save();
        }

        private void OnlyNumbersInTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Increment(object sender, RoutedEventArgs e)
        {
            int minIdVar = int.Parse(minId.Text);

            minId.Text = (minIdVar + 1).ToString();
        }

        private void Decrement(object sender, RoutedEventArgs e)
        {
            int minIdVar = int.Parse(minId.Text);

            if(!minId.Text.Equals("0"))
            {
                minId.Text = (minIdVar - 1).ToString();
            }
        }

        private void StartLoad()
        {
            this.IsEnabled = false;
            if (!worker.IsBusy)
            {
                worker.DoWork += Worker_DoWork;
                worker.ProgressChanged += Worker_ProgressChanged;
                worker.RunWorkerCompleted += Worker_RunWorkerCompleted;

                worker.WorkerReportsProgress = true;

                worker.RunWorkerAsync();
            }
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            worker.DoWork -= Worker_DoWork;
            worker.ProgressChanged -= Worker_ProgressChanged;
            worker.RunWorkerCompleted -= Worker_RunWorkerCompleted;

            this.IsEnabled = true;

            int minIdFilter = int.Parse(ConfigurationManager.AppSettings["MinId"]);

            foreach (ConnectUpFile file in connectUpfilesBw)
            {
                if (!this.ConnectUpFiles.Select(cf => cf.IdFile).Contains(file.IdFile))
                {
                    if(file.OperationId >= minIdFilter)
                    {
                        this.ConnectUpFiles.Add(file);
                    }
                }
                if(!this.ConnectUpFilesDilumat.Select(cf => cf.IdFile).Contains(file.IdFile))
                {
                    if (file.OperationId >= minIdFilter)
                    {
                        if (file.Instrument.Equals("Dilumat"))
                        {
                            this.ConnectUpFilesDilumat.Add(file);
                        }     
                    }
                }
                if (!this.ConnectUpFilesGeneup.Select(cf => cf.IdFile).Contains(file.IdFile))
                {
                    if (file.OperationId >= minIdFilter)
                    {
                        if (file.Instrument.Equals("GENE-UP"))
                        {
                            this.ConnectUpFilesGeneup.Add(file);
                        }  
                    }
                }
                if (!this.ConnectUpFilesTempo.Select(cf => cf.IdFile).Contains(file.IdFile))
                {
                    if (file.OperationId >= minIdFilter)
                    {
                        if (file.Instrument.Equals("TEMPO"))
                        {
                            this.ConnectUpFilesTempo.Add(file);
                        }                            
                    }
                }
                if (!this.ConnectUpFilesVidas.Select(cf => cf.IdFile).Contains(file.IdFile))
                {
                    if (file.OperationId >= minIdFilter)
                    {
                        if (file.Instrument.Equals("VIDAS"))
                        {
                            this.ConnectUpFilesVidas.Add(file);
                        }                           
                    }
                }
            }
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.pBar.Value = e.ProgressPercentage;
        }

        private void LoadFiles()
        {
            string instrumentsPath = ConfigurationManager.AppSettings["Path"];
            string ipGeneup = ConfigurationManager.AppSettings["IpGeneup"];
            string ipTempo = ConfigurationManager.AppSettings["IpTempo"];
            string ipVidas = ConfigurationManager.AppSettings["IpVidas"];

            IEnumerable<string> filesDilumat = null;
            IEnumerable<string> filesGENUP = null;
            IEnumerable<string> filesTEMPO = null;
            IEnumerable<string> filesVIDAS = null;

            this.connectUpfilesBw.Clear();
            int filesCount = 0;

            int totalFiles = 0;

            if (Directory.Exists(instrumentsPath + @"\Dilumat\ftproot\out"))
            {
                filesDilumat = Directory.EnumerateFiles(instrumentsPath + @"\Dilumat\ftproot\out");

                totalFiles = filesDilumat.Count();

                foreach (string file in filesDilumat)
                {
                    ConnectUpFile connectUpFile = new ConnectUpFile()
                    {
                        OperationId = int.Parse(GetDataInXml(File.ReadAllText(file), "operation_sample_id", true, false)),
                        Instrument = "Dilumat",
                        Analyse = GetDataInXml(File.ReadAllText(file), "program_type", true, false) + " - " + GetDataInXml(File.ReadAllText(file), "diluent_name", true, false),
                        Filename = file,
                    };

                    connectUpFile.IdFile = connectUpFile.OperationId + connectUpFile.Instrument;

                    connectUpfilesBw.Add(connectUpFile);
                    filesCount++;
                    this.ReportLoad(filesCount, totalFiles);
                }
            }

            if (Directory.Exists(instrumentsPath + @"\BCI\GENEUP_" + ipGeneup + "_REQUEST"))
            {
                 filesGENUP = Directory.EnumerateFiles(instrumentsPath + @"\BCI\GENEUP_" + ipGeneup + "_REQUEST");

                 totalFiles += filesGENUP.Count();

                foreach (string file in filesGENUP)
                {
                    for (int i = 0; i <= 1; i++)
                    {
                        bool isDoubleValue = true;
                        if (i == 0)
                        {
                            isDoubleValue = false;
                        }
                        if (GetDataInXml(File.ReadAllText(file), "<testIdentifier>", false, isDoubleValue).Length > 3)
                        {
                            break;
                        }
                        ConnectUpFile connectUpFile = new ConnectUpFile()
                        {
                            OperationId = int.Parse(GetDataInXml(File.ReadAllText(file), "<specimenIdentifier>", false, false)),
                            Instrument = "GENE-UP",
                            Analyse = GetDataInXml(File.ReadAllText(file), "<testIdentifier>", false, isDoubleValue),
                            Filename = file
                        };

                        connectUpFile.IdFile = connectUpFile.OperationId + connectUpFile.Analyse;

                        connectUpfilesBw.Add(connectUpFile);
                    }

                    filesCount++;
                    this.ReportLoad(filesCount, totalFiles);
                }
            }

            if (Directory.Exists(instrumentsPath + @"\BCI\TEMPO_" + ipTempo + "_REQUEST"))
            {
                filesTEMPO = Directory.EnumerateFiles(instrumentsPath + @"\BCI\TEMPO_" + ipTempo + "_REQUEST");

                totalFiles += filesTEMPO.Count();

                foreach (string file in filesTEMPO)
                {
                    for (int i = 0; i <= 1; i++)
                    {
                        bool isDoubleValue = true;
                        if (i == 0)
                        {
                            isDoubleValue = false;
                        }
                        if (GetDataInXml(File.ReadAllText(file), "<testIdentifier>", false, isDoubleValue).Length > 3)
                        {
                            break;
                        }
                        ConnectUpFile connectUpFile = new ConnectUpFile()
                        {
                            OperationId = int.Parse(GetDataInXml(File.ReadAllText(file), "<patientIdentifier>", false, false)),
                            Instrument = "TEMPO",
                            Analyse = GetDataInXml(File.ReadAllText(file), "<testIdentifier>", false, isDoubleValue),
                            Filename = file,
                            InstrumentSectionId = GetDataInXml(File.ReadAllText(file), "<instrumentSectionId>", false, isDoubleValue)
                        };

                        connectUpFile.IdFile = connectUpFile.OperationId + connectUpFile.Analyse;

                        connectUpfilesBw.Add(connectUpFile);              
                    }

                    filesCount++;
                    this.ReportLoad(filesCount, totalFiles);
                }
            }

            if (Directory.Exists(instrumentsPath + @"\BCI\VIDAS_" + ipVidas + "_REQUEST"))
            {
                filesVIDAS = Directory.EnumerateFiles(instrumentsPath + @"\BCI\VIDAS_" + ipVidas + "_REQUEST");

                totalFiles += filesVIDAS.Count();

                foreach (string file in filesVIDAS)
                {
                    for (int i = 0; i <= 1; i++)
                    {
                        bool isDoubleValue = true;
                        if (i == 0)
                        {
                            isDoubleValue = false;
                        }
                        if (GetDataInXml(File.ReadAllText(file), "<testIdentifier>", false, isDoubleValue).Length > 3)
                        {
                            break;
                        }
                        ConnectUpFile connectUpFile = new ConnectUpFile()
                        {
                            OperationId = int.Parse(GetDataInXml(File.ReadAllText(file), "<patientIdentifier>", false, false)),
                            Instrument = "VIDAS",
                            Analyse = GetDataInXml(File.ReadAllText(file), "<testIdentifier>", false, false),
                            Filename = file
                        };

                        connectUpFile.IdFile = connectUpFile.OperationId + connectUpFile.Analyse;

                        connectUpfilesBw.Add(connectUpFile);
                    }

                    filesCount++;
                    this.ReportLoad(filesCount, totalFiles);
                }
            }
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            LoadFiles();
        }

        private void ReportLoad(int files, int count)
        {
            int purcent = (int)((float)files / (float)count * 100f);
            worker.ReportProgress(purcent);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            minId.Text = ConfigurationManager.AppSettings["MinId"].ToString();
            StartLoad();
        }

        private void OpenOptionWindow(object sender, RoutedEventArgs e)
        {
            Option opt = new Option();
            opt.Show();
            this.Close();
        }

        private void GenerateFile(object sender, RoutedEventArgs e)
        {

            if (SelectedRow?.Equals(null) != null)
            {
                string xmlFile = File.ReadAllText(SelectedRow.Filename);

                string pathTemplate = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

                var dilumatTemplate = XDocument.Load(pathTemplate + @"\dilumatTemplate.xml");
                var GeneupTemplate = XDocument.Load(pathTemplate + @"\geneup_Template.xml");
                var TempoTemplate = XDocument.Load(pathTemplate + @"\tempoTemplate.xml");
                var VidasTemplate = XDocument.Load(pathTemplate + @"\vidasTemplate.xml");

                XDocument template = new XDocument();
                XDocument newXmlFile = new XDocument();

                string instrumentsPath = ConfigurationManager.AppSettings["Path"];
                string ipGeneup = ConfigurationManager.AppSettings["IpGeneup"];
                string ipTempo = ConfigurationManager.AppSettings["IpTempo"];
                string ipVidas = ConfigurationManager.AppSettings["IpVidas"];

                if (instrumentsPath.Equals(""))
                {
                    string noInstrumentsPath = "Path of the folder ''Instruments'' is required !";
                    string caption = "Error";
                    MessageBoxButton button = MessageBoxButton.OK;
                    MessageBoxImage icon = MessageBoxImage.Warning;
                    System.Windows.MessageBox.Show(noInstrumentsPath, caption, button, icon);
                    return;
                }

                var filePath = "";
                if (SelectedRow.Instrument == "Dilumat") //return machineName_result_operationSampleId_hhmmssmil.xml
                {
                    filePath = instrumentsPath + @"\Dilumat\ftproot\in\31165612_result_" + SelectedRow.OperationId + "_" + DateTime.Now.ToString("HHmmssfff") + ".xml";
                    template = dilumatTemplate;
                    newXmlFile = TransformXml(template, "Dilumat", xmlFile);
                }
                else if (SelectedRow.Instrument == "GENE-UP") //return Gene-UP-Routine_yyMMdd_hh+2mmss_iteration.xml
                {
                    if (!ipGeneup.Equals(""))
                    {
                        DateTime dateValue = DateTime.Now;

                        dateValue = dateValue.AddHours(2);

                        filePath = instrumentsPath + @"\BCI\GENEUP_" + ipGeneup + @"_RESULT\Gene-UP-Routine_" + DateTime.Now.ToString("yyMMdd") + "_" + dateValue.ToString("HHmmss") + "_0.xml";
                        template = GeneupTemplate;
                        newXmlFile = TransformXml(template, "GENE-UP", xmlFile);

                        int iteration = 0;

                        while (File.Exists(filePath))
                        {
                            iteration++;
                            filePath = instrumentsPath + @"\BCI\GENEUP_" + ipGeneup + @"_RESULT\Gene-UP-Routine_" + DateTime.Now.ToString("yyMMdd") + "_" + dateValue.ToString("HHmmss") + "_" + iteration + ".xml";
                        }
                    }
                    else
                    {
                        string noIpAdressForGeneup = "IP address for GENE-UP is required !";
                        string caption = "Error";
                        MessageBoxButton button = MessageBoxButton.OK;
                        MessageBoxImage icon = MessageBoxImage.Warning;
                        System.Windows.MessageBox.Show(noIpAdressForGeneup, caption, button, icon);
                        return;
                    }
                }
                else if (SelectedRow.Instrument == "TEMPO") //return InstrumentSectionId_yyMMdd_hhmmss_iteration.xml
                {
                    if (!ipTempo.Equals(""))
                    {
                        filePath = instrumentsPath + @"\BCI\TEMPO_" + ipTempo + @"_RESULT\" + SelectedRow.InstrumentSectionId + "_" + DateTime.Now.ToString("yyMMdd") + "_" + DateTime.Now.ToString("HHmmss") + "_0.xml";
                        template = TempoTemplate;
                        newXmlFile = TransformXml(template, "TEMPO", xmlFile);

                        int iteration = 0;

                        while (File.Exists(filePath))
                        {
                            iteration++;
                            filePath = instrumentsPath + @"\BCI\TEMPO_" + ipTempo + @"_RESULT\" + SelectedRow.InstrumentSectionId + "_" + DateTime.Now.ToString("yyMMdd") + "_" + DateTime.Now.ToString("HHmmss") + "_" + iteration + ".xml";
                        }
                    }
                    else
                    {
                        string noIpAdressForTempo = "IP address for TEMPO is required !";
                        string caption = "Error";
                        MessageBoxButton button = MessageBoxButton.OK;
                        MessageBoxImage icon = MessageBoxImage.Warning;
                        System.Windows.MessageBox.Show(noIpAdressForTempo, caption, button, icon);
                        return;
                    }     
                    
                }
                else if (SelectedRow.Instrument == "VIDAS") //return InstrumentSectionId_yyMMdd_hhmmss_iteration.xml
                {
                    if (!ipVidas.Equals(""))
                    {
                        filePath = instrumentsPath + @"\BCI\VIDAS_" + ipVidas + @"_RESULT\" + "VIDASSP01_" + DateTime.Now.ToString("yyMMdd") + "_" + DateTime.Now.ToString("HHmmss") + "_0.xml";
                        template = VidasTemplate;
                        newXmlFile = TransformXml(template, "VIDAS", xmlFile);

                        int iteration = 0;

                        while (File.Exists(filePath))
                        {
                            iteration++;
                            filePath = instrumentsPath + @"\BCI\VIDAS_" + ipVidas + @"_RESULT\" + SelectedRow.InstrumentSectionId + "_" + DateTime.Now.ToString("yyMMdd") + "_" + DateTime.Now.ToString("HHmmss") + "_" + iteration + ".xml";
                        }
                    }
                    else
                    {
                        string noIpAdressForVidas = "IP address for VIDAS is required !";
                        string caption = "Error";
                        MessageBoxButton button = MessageBoxButton.OK;
                        MessageBoxImage icon = MessageBoxImage.Warning;
                        System.Windows.MessageBox.Show(noIpAdressForVidas, caption, button, icon);
                        return;
                    }

                }
                using (TextWriter writer = File.CreateText(filePath))
                {
                    writer.WriteLine(template);
                }

                string XmlGenerated = "XML is generated succesfully ! \n \n" + filePath;
                string caption2 = "Success";
                MessageBoxButton button2 = MessageBoxButton.OK;
                MessageBoxImage icon2 = MessageBoxImage.Information;
                System.Windows.MessageBox.Show(XmlGenerated, caption2, button2, icon2);
            }
            else
            {
                string nothingchoosed = "Choose a file in the Table !";
                string caption = "Error";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxImage icon = MessageBoxImage.Warning;
                System.Windows.MessageBox.Show(nothingchoosed, caption, button, icon);
            }

        }

        private string GetDataInXml(string xmlFile, string nameOfValue, bool isDilumat, bool isDoubleValue)
        {
            int index = xmlFile.IndexOf(nameOfValue);

            if (isDoubleValue)
            {
                index = xmlFile.IndexOf(nameOfValue, index + 1);
            }

            int indexEnd = 0;

            string stop = "<";

            if (isDilumat)
            {
                index = index + nameOfValue.Length + 2;
                stop = " " ;
                indexEnd--;

               
            }
            else
            {
                index = index + nameOfValue.Length;
            }

            for (var i = 0; i <= xmlFile.Length; i++)
            {
                if (nameOfValue == "diluent_name")
                {
                    Regex lowerCase = new Regex("[a-z]");

                    if (!xmlFile.Substring(index + i, 1).Equals(stop) || !lowerCase.IsMatch(xmlFile.Substring(index + i + 1, 1)))
                    {
                        indexEnd++;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    if (!xmlFile.Substring(index + i, 1).Equals(stop))
                    {
                        indexEnd++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            string data = xmlFile.Substring(index, indexEnd);

            return data;
        }

        private XDocument SetDataInXml(XDocument template, string[] namesValues, string[] valuesToSet, bool isDilumat)
        {
            if (isDilumat)
            {
                for (int i = 0; i < valuesToSet.Length; i++)
                {
                    XElement root = template.Element("root");
                    root.Attribute(namesValues[i]).Value = valuesToSet[i];
                }
            }
            else
            {
                for (int i = 0; i < valuesToSet.Length; i++)
                {
                    XElement element = template.XPathSelectElement(namesValues[i]);
                    element.Value = valuesToSet[i];
                }
            }

            return template;
        }

        private XDocument TransformXml(XDocument template, string type, string oldXmlFile)
        {
            XDocument newXmlFile = template;

            if (type == "Dilumat")
            {
                int program_sample_min_weight = int.Parse(GetDataInXml(oldXmlFile, "program_sample_min_weight", true, false));

                var program_sample_max_weight = int.Parse(GetDataInXml(oldXmlFile, "program_sample_max_weight", true, false));

                Random rnd = new Random();
                double operation_mass_before = rnd.NextDouble() * (program_sample_max_weight - program_sample_min_weight) + program_sample_min_weight;
                operation_mass_before = Math.Round(operation_mass_before, 2);

                string system_date_time_gmt = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff");

                int operation_sample_id = SelectedRow.OperationId;

                string operation_analysis_id = operation_sample_id.ToString();

                string program_type = SelectedRow.Analyse;

                int program_target = int.Parse(GetDataInXml(oldXmlFile, "program_target", true, false));

                double differenceNumber = 0.2;
            
                double min_factor = program_target - differenceNumber;
                double max_factor = program_target + differenceNumber;

                double operation_real_dilution_factor = rnd.NextDouble() * (max_factor - min_factor) + min_factor;
                operation_real_dilution_factor = Math.Round(operation_real_dilution_factor, 2);

                double operation_mass_final = (operation_mass_before * operation_real_dilution_factor);
                operation_mass_final = Math.Round(operation_mass_final, 2);

                double operation_total_mass_dispensed = (operation_mass_final - operation_mass_before);
                operation_total_mass_dispensed = Math.Round(operation_total_mass_dispensed, 2);

                double operation_mass_dispensed = operation_total_mass_dispensed;

                string diluent_name = GetDataInXml(oldXmlFile, "diluent_name", true, false);

                int diluent_batch_number = rnd.Next(1000000);

                string[] namesValues =
                {
                    "program_sample_min_weight",
                    "program_sample_max_weight",
                    "operation_mass_before",
                    "system_date_time_gmt",
                    "operation_sample_id",
                    "operation_analysis_id",
                    "program_type",
                    "program_target",
                    "operation_real_dilution_factor",
                    "operation_mass_final",
                    "operation_total_mass_dispensed",
                    "operation_mass_dispensed",
                    "diluent_name",
                    "diluent_batch_number"
                };

                string[] valuesToSet =
                {
                    program_sample_min_weight.ToString(),
                    program_sample_max_weight.ToString(),
                    operation_mass_before.ToString(),
                    system_date_time_gmt,
                    operation_sample_id.ToString(),
                    operation_analysis_id,
                    program_type,
                    program_target.ToString(),
                    operation_real_dilution_factor.ToString(),
                    operation_mass_final.ToString(),
                    operation_total_mass_dispensed.ToString(),
                    operation_mass_dispensed.ToString(),
                    diluent_name,
                    diluent_batch_number.ToString()
                };

                newXmlFile = SetDataInXml(newXmlFile, namesValues, valuesToSet, true);

            }
            else if (type == "GENE-UP")
            {
                DateTime dateValue = DateTime.Now;

                string dateTime = dateValue.ToString("yyyyddMMHHmmss");

                string resultDateTime = dateValue.Add(new TimeSpan(0, -2, 0)).ToString("yyyyddMMHHmmss");

                string testIdentifier = SelectedRow.Analyse;

                string AWOSID = GetDataInXml(oldXmlFile, "<AWOSID>", false, false);

                if (SelectedRow.Analyse != GetDataInXml(oldXmlFile, "<testIdentifier>", false, false))
                {
                    AWOSID = GetDataInXml(oldXmlFile, "<AWOSID>", false, true);
                }

                int specimenIdentifier = SelectedRow.OperationId;

                string[] namesValues =
                {
                    "aiMessage/header/dateTime",
                    "aiMessage/specimen/specimenIdentifier",
                    "aiMessage/observationResult/resultDateTime",
                    "aiMessage/observationOrder/AWOSID",
                    "aiMessage/observationOrder/universalIdentifier/testIdentifier"
                };

                string[] valuesToSet =
                {
                    dateTime,
                    specimenIdentifier.ToString(),
                    resultDateTime,
                    AWOSID,
                    testIdentifier
                };

                newXmlFile = SetDataInXml(newXmlFile, namesValues, valuesToSet, false);
            }
            else if (type == "TEMPO")
            {
                DateTime dateValue = DateTime.Now;

                string dateTime = dateValue.ToString("yyyyddMMHHmmss");

                string patientIdentifier = SelectedRow.OperationId.ToString();

                string specimenIdentifier = patientIdentifier.ToString();

                string testIdentifier = SelectedRow.Analyse;

                Random rnd = new Random();
                int value = rnd.Next(0, 10);

                string instrumentName = SelectedRow.InstrumentSectionId;

                string[] namesValues =
                {
                    "aiMessage/header/dateTime",
                    "aiMessage/requestResult/patientInformation/patientIdentifier",
                    "aiMessage/requestResult/testOrder/specimen/specimenIdentifier",
                    "aiMessage/requestResult/testOrder/test/universalIdentifier/testIdentifier",
                    "aiMessage/header/instrumentName",
                    "aiMessage/requestResult/testOrder/test/result/value"
                };

                string[] valuesToSet =
                {
                    dateTime,
                    patientIdentifier,
                    specimenIdentifier,
                    testIdentifier,
                    instrumentName,
                    value.ToString()
                };

                newXmlFile = SetDataInXml(newXmlFile, namesValues, valuesToSet, false);
            }

            else if (type == "VIDAS")
            {
                string senderName = GetDataInXml(oldXmlFile, "<senderName>", false, false);

                string version = GetDataInXml(oldXmlFile, "<version>", false, false);

                DateTime dateValue = DateTime.Now;

                string dateTime = dateValue.ToString("yyyyddMMHHmmss");

                string patientIdentifier = SelectedRow.OperationId.ToString();

                string lastName = GetDataInXml(oldXmlFile, "<lastName>", false, false);

                string specimenIdentifier = patientIdentifier.ToString();

                string testIdentifier = SelectedRow.Analyse;

                Random rnd = new Random();
                double value = rnd.NextDouble() * (0.1 - 0) + 0;
                value = Math.Round(value, 2);


                string resultDateTime = dateValue.ToString("yyyyddMMHHmmss");

                string[] namesValues =
                {
                    "aiMessage/header/senderName",
                    "aiMessage/header/version",
                    "aiMessage/header/dateTime",
                    "aiMessage/requestResult/patientInformation/patientIdentifier",
                    "aiMessage/requestResult/patientInformation/lastName",
                    "aiMessage/requestResult/testOrder/specimen/specimenIdentifier",
                    "aiMessage/requestResult/testOrder/test/universalIdentifier/testIdentifier",
                    "aiMessage/requestResult/testOrder/test/result/value",
                    "aiMessage/requestResult/testOrder/test/result/resultDateTime"
                };

                string[] valuesToSet =
                {
                    senderName,
                    version,
                    dateTime,
                    patientIdentifier,
                    lastName,
                    specimenIdentifier,
                    testIdentifier,
                    value.ToString(),
                    resultDateTime
                };

                newXmlFile = SetDataInXml(newXmlFile, namesValues, valuesToSet, false);
            }

            return newXmlFile;
        }

        public void Save()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            config.AppSettings.Settings["MinId"].Value = minId.Text;

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
